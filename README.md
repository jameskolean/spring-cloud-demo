#Run
Start each service in order

```
mvn spring-boot:run -pl config-server
mvn spring-boot:run -pl discovery
mvn spring-boot:run -pl gateway
mvn spring-boot:run -pl book-service
mvn spring-boot:run -pl rating-service
```

####Verify that you can connect to the services directly

```
curl http://localhost:7085/books
curl http://localhost:7084/ratings
```

####Now test using the gateway

```
curl http://localhost:7080/booking-service/books
curl http://localhost:7080/rating-service/ratings
```

####View traces in Zipkin
```
http:\\localhost:5511\search
```


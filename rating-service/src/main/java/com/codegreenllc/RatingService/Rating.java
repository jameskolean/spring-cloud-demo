package com.codegreenllc.RatingService;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Rating {
	private Long bookId;
	private Long id;
	private int stars;
}

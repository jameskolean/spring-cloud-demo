package com.codegreenllc.RatingService;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@EnableEurekaClient
@RestController
@RequestMapping("/ratings")
public class RatingServiceApplication {

	public static void main(final String[] args) {
		SpringApplication.run(RatingServiceApplication.class, args);
	}

	private final List<Rating> ratingList = Arrays.asList(Rating.builder().id(1L).bookId(1L).stars(5).build(),
			Rating.builder().id(2L).bookId(2L).stars(4).build(), Rating.builder().id(3L).bookId(3L).stars(3).build(),
			Rating.builder().id(4L).bookId(4L).stars(2).build(), Rating.builder().id(5L).bookId(5L).stars(1).build());

	@GetMapping("")
	public List<Rating> findAllRatings() {
		return ratingList;
	}

	@GetMapping("/{bookId}")
	public List<Rating> findRatingsByBookId(@PathVariable final Long bookId) {
		return bookId == null || bookId.equals(0L) ? Collections.EMPTY_LIST
				: ratingList.stream().filter(r -> r.getBookId().equals(bookId)).collect(Collectors.toList());
	}
}
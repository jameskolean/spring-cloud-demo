package com.codegreenllc.BookService;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@EnableEurekaClient
@RestController
@RequestMapping("/books")
public class BookServiceApplication {
	private static final Log log = LogFactory.getLog(BookServiceApplication.class);

	public static void main(final String[] args) {
		SpringApplication.run(BookServiceApplication.class, args);
	}

	private final List<Book> bookList = Arrays.asList(
			Book.builder().id(1L).title("Watchmen").author("Alan Moore").build(),
			Book.builder().id(2L).title("The Color of Magic").author("Terry Pratchett").build());

	@GetMapping("")
	public List<Book> findAllBooks() {
		log.info("@findAllBooks");
		return bookList;
	}

	@GetMapping("/{bookId}")
	public Book findBook(@PathVariable final Long bookId) {
		return bookList.stream().filter(b -> b.getId().equals(bookId)).findFirst().orElse(null);
	}
}
package com.codegreenllc.BookService;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Book {
	private String author;
	private Long id;
	private String title;
}